import java.sql.*;

public class main {
    public static void main(String[] args) throws SQLException {
        try {
            DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        String dbURL = "jdbc:sqlserver://localhost;database=BikeStores;user=rpuser;password=1234";
        Connection conn = DriverManager.getConnection(dbURL);
        if (conn != null) {
            System.out.println("Connected");
        }

        Statement stmt = conn.createStatement();
        ResultSet result = stmt.executeQuery("SELECT * FROM production.brands");

        while ( result.next() ) {
            String brand_name = result.getString("brand_name");
            String brand_id = result.getString("brand_id");
            System.out.println(brand_id +"  "+ brand_name);
        }
    }

}